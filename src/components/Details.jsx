import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

function Details() {
  const { id } = useParams();
  const movieDetails = useSelector(state => state.data.movies);
  //const dispatch = useDispatch();
  const  getMoviesData = movieDetails.find(e=>e.id === parseInt(id))
  const handleCardClick = (imdbUrl) => {
    window.open(imdbUrl);
  };

  return (
    <div>
      <center>
      {getMoviesData && 
      <div>
       <div>
      <img src={getMoviesData.image} alt={getMoviesData.movie} />
      <h2>{getMoviesData.movie}</h2>
      <p>Rating: {getMoviesData.rating}</p>
      <button onClick={() => handleCardClick(getMoviesData.imdb_url)} className="btn btn-success" > Watch Now </button>
    </div>
    </div>}
    </center>   
    </div>
  );
}

export default Details;







