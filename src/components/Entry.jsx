import React from 'react'
import Navbar from './Navbar'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import WatchLater from './WatchLater';
import Movies from './Movies';
import Details from './Details';


export default function Entry() {
  return (
    <div>
      <Router>
      <Navbar/>
        <Routes>
          <Route path="/" element={<Movies/>} /> 
          <Route path="/movies" element={<Movies/>} /> 
          {/* <Route path="/movie/:movieName" element={<Details />} /> */}
          <Route path="/movies/:id/:movie" element={<Details/>} />
          <Route path="/WatchLater" element={<WatchLater/>} /> 
          
        </Routes>  
      </Router>
    </div>
  )
}
