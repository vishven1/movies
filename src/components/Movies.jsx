import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchMovies, addToWatchLater } from '../reducer/MovieSlice';
import { Link } from 'react-router-dom';

function Movies() {
  const [searchTerm, setSearchTerm] = useState('');
  const [pageSize, setPageSize] = useState(5);
  const [currentPage, setCurrentPage] = useState(1);
  const [hoveredCard, setHoveredCard] = useState(null);


  const movies = useSelector(state => state.data.movies);
  const watchLater = useSelector(state => state.data.watchLater);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovies());
  }, [dispatch]);

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);
    const page = parseInt(params.get('page')) || 1;
    setCurrentPage(page);
  }, []);

  useEffect(() => {
    setCurrentPage(1);
  }, [searchTerm]);

  useEffect(() => {
    const params = new URLSearchParams();
    params.set('page', currentPage.toString());
    window.history.replaceState(null, '', `${window.location.pathname}?${params.toString()}`);
  }, [currentPage]);

  const filteredMovies = movies.filter(movie =>
    movie.movie.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const indexOfLastMovie = currentPage * pageSize;
  const indexOfFirstMovie = indexOfLastMovie - pageSize;
  const currentMovies = filteredMovies.slice(indexOfFirstMovie, indexOfLastMovie);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  const handlePageSizeChange = (e) => {
    setPageSize(parseInt(e.target.value));
    setCurrentPage(1);
  };

  const isMovieInWatchLater = (movie) => {
    return watchLater.find(item => item.id === movie.id);
  };

  const handleAddToWatchLater = (movie, event) => {
    event.stopPropagation();
    dispatch(addToWatchLater(movie));
  };

  const handleCardClick = (movieId) => {
    return <Link to={`/movie/${movieId}`} />;
  };

  return (
    <>
      <div className="container">
        <div className="row mb-3">
          <div className="col">
            <br />
            <input
              type="text"
              className="form-control"
              placeholder="Search movies..."
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}/>
          </div>
          <div className="col-auto">
            <label htmlFor="pageSize">Movies per page:</label>
            <select
              id="pageSize"
              className="form-control"
              value={pageSize}
              onChange={handlePageSizeChange}>
              <option value={5}>5</option>
              <option value={10}>10</option>
              <option value={15}>15</option>
            </select>
          </div>
        </div>
        <div className="row">
          {currentMovies.map(movie => (
            <div  key={movie.id}  className="col-12 col-md-6 col-lg-3 mb-3"
              style={{
                cursor: 'pointer',
                boxShadow: hoveredCard === movie.id ? '0 0 10px rgba(0,0,0,0.3)' : 'none',
                transition: 'box-shadow 0.3s'}}
              onMouseEnter={() => setHoveredCard(movie.id)}
              onMouseLeave={() => setHoveredCard(null)}>
              <div className="card">
                <Link to={`/movies/${movie.id}/${movie.movie.toLowerCase().replace(/\s+/g, '-')}}`}>
                <video src={movie.image} className="card-img-top" alt={movie.movie} />
                </Link>
                <div className="card-body">
                  <h5 className="card-title">{movie.movie}</h5>
                  <p>Rating: {movie.rating}</p>
                  {isMovieInWatchLater(movie) ? (
                    <p className="text-success">Added to Watch Later</p>
                  ) : (
                    <button className="btn btn-primary" onClick={(e) => handleAddToWatchLater(movie, e)}>Add to Watch Later</button>
                  )}
                </div>
              </div>
            </div>
          ))}
        </div>
        <div className="row">
          <div className="col">
            <nav aria-label="Pagination">
              <ul className="pagination justify-content-center">
                {Array.from({ length: Math.ceil(filteredMovies.length / pageSize) }, (_, i) => (
                  <li key={i} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                    <button className="page-link" onClick={() => paginate(i + 1)}>{i + 1}</button>
                  </li>
                ))}
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </>
  );
}

export default Movies;

