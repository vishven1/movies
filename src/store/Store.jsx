import { configureStore } from '@reduxjs/toolkit'
import MovieSlice from '../reducer/MovieSlice'

export const store = configureStore({
  reducer: {
    data: MovieSlice,
    },
})
